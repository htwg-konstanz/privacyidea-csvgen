#!/bin/env python
# Fair License (Fair)
# Copyright (c) 2023 tzink _at_htwg-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
# [https://opensource.org/licenses/Fair/](https://opensource.org/licenses/Fair/)

import pyotp
import time
import datetime
import qrcode
from urllib.parse import urlparse, parse_qs
import base64
import json
import random
import string
import sys
import os
import errno
import codecs
import argparse
import csv
import sqlite3
import uuid
import hashlib

# secret : base32 encoded secret key
# seed : hex encoded secret key

def parse_args():
    parser = argparse.ArgumentParser(description="PIDEA csv token creator", add_help=True)

    parser.add_argument('-c', '--count', type=int, default=1, help='Number of tokens to generate', required=False)
    parser.add_argument('-p', '--path', type=str, default=os.getcwd(), help='A filesystem path to an output directory', required=False)
    parser.add_argument('-d', '--db', type=str, default=None, help='sqlite Database file', required=False)
    parser.add_argument('-t', '--type', type=str, default="totp", help='Token type: totp, hotp, tan. Default: totp', required=False)
    parser.add_argument('-i', '--issuer', type=str, default="HTWG-Konstanz", help='Issuer Name (OTP only)', required=False)
    parser.add_argument('-a', '--account', type=str, default="privacyIdea", help='Account Name', required=False)
    parser.add_argument('-n', '--number', type=int, default="10", help='Number of TANs per token (only with TAN type)', required=False)

    args = parser.parse_args()

    # Check if the output directory exists
    if not os.path.exists(args.path):
        print(f"Error: Output directory '{args.path}' does not exist.", file=sys.stderr)
        parser.print_help()
        exit(1)

    # Check if count is a positive integer
    if args.count <= 0:
        print("Error: Count must be a positive integer.", file=sys.stderr)
        parser.print_help()
        exit(1)

    # check if db exists
    if args.db and not os.path.exists(args.db):
        print(f"Error: database '{args.db}' not found.", file=sys.stderr)
        parser.print_help()
        exit(1)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    
    return args


def generate_serial(ttype:str, dbpath=None):
    #serial = ttype.upper() + "".join([random.choice(string.hexdigits) for x in range(8)]).upper()
    uid = hashlib.blake2b(str.encode(uuid.uuid4().hex), digest_size=8)
    serial = ttype.upper() + uid.hexdigest().upper()
    if dbpath:
        conn = database_connect(dbpath)
        cursor = conn.cursor()
        # Check if generated serial already exists in db, if yes regenerate
        while cursor.execute('SELECT EXISTS(SELECT 1 FROM tokens WHERE serial=?)', (serial,)).fetchone()[0]:
            uid = hashlib.blake2b(str.encode(uuid.uuid4().hex), digest_size=8)
            serial = prefix.upper() + uid.hexdigest().upper()
        conn.close()
    return serial

def generate_secret():
    secret = pyotp.random_base32()
    seed = b32_to_hex(secret)
    return (secret, seed)

def create_uri(secret,ttype,account="",issuer=""):
    uri = ""
    if ttype == "totp":
        uri = pyotp.totp.TOTP(secret).provisioning_uri(name=account, issuer_name=issuer)
    elif ttype == "hotp":
        uri = pyotp.hotp.HOTP(secret).provisioning_uri(name=account, issuer_name=issuer)
    return uri

def create_qrcode(uri):
    qr = qrcode.QRCode() # or set image_factory, test for print
    qr.add_data(uri)
    qr.make()
    return qr

# this would be better as class (or even meta) with constructors, also export with .as..()
def create_token(args):
    token: dict = {}
    token['serial'] = generate_serial(args.type, args.db)
    token['type'] = args.type
    if args.type == "tan":
        # <serial>, <seed=n/a>, type=TAN, <space-separated list of tans>
        token['secret'] = ""
        token['seed'] = ""
        #token['tans'] = " ".join(str(x) for x in [random.randrange(100000, 999999) for i in range(args.number)])
        token['tans'] = " ".join(map(str,[random.randrange(100000, 999999) for i in range(args.number)]))
    elif args.type in ["totp","hotp"]:
        secret, seed = generate_secret()
        token['secret'] = secret
        token['seed'] = seed
        token['otp_len'] = 6
        token['uri'] = create_uri(secret, args.type,args.account,args.issuer)
        token['qr'] = create_qrcode(token['uri'])
        if args.type == "totp":
            # <serial>, <seed>, TOTP, <otp length>, <time step>
            token['time_step'] = 30
        elif args.type == "hotp":
            # <serial>, <seed>, [HOTP, <otp length>, <counter>]
            token['counter'] = 0
    return token

def save_qrcode_image(token, path):
    if os.path.isdir(path):
        fname = os.path.join(path,"%s.png" %token['serial'])
        img = token['qr'].make_image()
        img.save(fname)
        print(f"qrcode {fname} saved", file=sys.stderr)
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT,path))
    return fname

# parse uri and return encoded info
def parse_uri(uri):
    parsed_uri = urlparse(uri)
    params = parse_qs(parsed_uri.query)
    #secret = params.get('secret', [])[0]
    return params

# parse pidea response json (untested)
def parse_response(json_response):
    response_data = json.loads(json_response)
    # Extract the base64-encoded image data from the 'img' field
    img_tag = response_data['detail']['googleurl']['img']
    base64_start = img_tag.find('base64,') + len('base64,')
    img_base64 = img_tag[base64_start:-2] # could be 0, depending on actual json
    img_binary = base64.b64decode(base64_img)
    # save image to file
    #with open('output.png', 'wb') as img_file:
    #    img_file.write(img_binary)

def verify_otp_code(secret):
    totp = pyotp.TOTP(secret)
    code = totp.now()
    time_remaining = totp.interval - datetime.datetime.now().timestamp() % totp.interval
    print(code, totp.verify(code), time_remaining)

def b32_to_hex(secret):
    return base64.b32decode(secret).hex()

def hex_to_b32(seed):
    return base64.b32encode(codecs.decode(seed,'hex'))
 
def create_csv_record(token):
    record: list = []
    if token['type'] == 'totp':
        record = [ token['serial'], token['seed'], token['type'], token['otp_len'], token['time_step'] ]
    elif token['type'] == 'hotp':
        record = [ token['serial'], token['seed'], token['type'], token['otp_len'], token['counter'] ]
    elif token['type'] == 'tan':
        record = [ token['serial'], token['seed'], token['type'], token['tans'] ]
    return record 

def write_to_csv(rows, csv_file_path):
    with open(csv_file_path, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        for row in rows:
            csv_writer.writerow(row)
    print(f"csv file '{csv_file_path}' created.", file=sys.stderr)

def database_connect(dbpath):
    conn = sqlite3.connect(dbpath)
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS tokens (
            serial TEXT PRIMARY KEY,
            seed TEXT,
            type TEXT,
            otp_len INT,
            time_step INT
        )
    ''')
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS uris (
            serial TEXT PRIMARY KEY,
            uri TEXT
        )
    ''')
    conn.commit()
    return conn

def write_to_database(dbpath, token):
    if dbpath:
        conn = database_connect(dbpath)
        cursor = conn.cursor()
        cursor.execute('INSERT INTO tokens (serial, seed, type, otp_len, time_step) VALUES (?, ?, ?, ?, ?)', (token['serial'], token['seed'], token['type'], token['otp_len'], token['time_step']))
        cursor.execute('INSERT INTO uris (serial, uri) VALUES (?, ?)', (token['serial'], token['uri']))
        conn.commit()
        conn.close()


def test():
    """
    example_json_response = '''
    {
       "detail": {
         "googleurl": {
           "description": "URL for Authenticator",
           "img": "<img width=250 src=\\"data:image/png;base64,AAHCAQAAAABUY/ToAAADsUlE\\"/>",
           "value": "otpauth://totp/[mylabel]?secret=[secret]"
         }
       }
    }
    '''
    parse_response(example_json_response)
    """
    #args = parse_args()
    #token = create_token(args)
    serial = generate_serial("totp")
    secret, seed = generate_secret()
    uri = create_uri(secret, "totp")
    qr = create_qrcode(uri)
    params = parse_uri(uri)
    #record = create_csv_record(serial,secret)

    print(secret, file=sys.stderr)
    print(uri, file=sys.stderr)
    #print(params)
    qr.print_ascii()
    print(record)
    verify_otp_code(secret)


def main():
    args = parse_args()
    #print(f"Count: {args.count}")
    #print(f"Output Path: {args.path}")
    valid_to = datetime.datetime.now() + datetime.timedelta(days=90)
    token_rows = [] # serial,seed,type,otp_len,time_step
    #letter_rows = [] # serial,path,valid_to
    #letter_rows.append(["serial","path","validto"])

    for i in range(args.count):
        token = create_token(args)
        record = create_csv_record(token)
        token_rows.append(record)
        print(",".join(map(str,record)))
        if token['type'] in ['totp', 'hotp']:
            img_path = save_qrcode_image(token, args.path)
        #letter_rows.append([ token['serial'],os.path.abspath(img_path),valid_to.strftime('%Y-%m-%d') ])
        write_to_database(args.db, token)
    token_file_path = os.path.join(args.path, 'token-import.csv')
    write_to_csv(token_rows, token_file_path)
    #letter_file_path = os.path.join(args.path, 'letter-data.csv')
    #write_to_csv(letter_rows, letter_file_path)

if __name__ == "__main__":
    #test()
    main()

